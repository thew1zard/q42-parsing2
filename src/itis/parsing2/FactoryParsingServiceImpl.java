package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {
        File folder = new File(factoryDataDirectoryPath);
        String line;
        HashMap<String, String> parsedFactoryData = new HashMap<>();
        BufferedReader bufferedReader = null;
        try {
            Class<?> factoryClass = Class.forName(Factory.class.getName());
            HashSet<String> requiredParsingData = new HashSet<>();
            for (Field f : factoryClass.getDeclaredFields()) {
                requiredParsingData.add(f.getName());
            }
            for (Field f : factoryClass.getDeclaredFields()) {
                if (f.isAnnotationPresent(Concatenate.class)) {
                    requiredParsingData.addAll(Arrays.asList(f.getAnnotation(Concatenate.class).fieldNames()));
                }
            }
            for (File f : Objects.requireNonNull(folder.listFiles())) {
                ArrayList<String> departments = new ArrayList<>();
                bufferedReader = new BufferedReader(new FileReader(f));
                while ((line = bufferedReader.readLine()) != null) {
                    if (line.equals("---")) {
                        continue;
                    }
                    String[] splittedDataLine = line.split(":");
                    splittedDataLine[0] = splittedDataLine[0].replaceAll("\"", "");
                    if(splittedDataLine.length == 1){
                        parsedFactoryData.put(splittedDataLine[0], null);
                        continue;
                    }
                    splittedDataLine[1] = splittedDataLine[1].replaceAll(" \"", "");
                    splittedDataLine[1] = splittedDataLine[1].replaceAll("\"", "");
                    if (splittedDataLine[0].equals("departments")) {
                        splittedDataLine[1] = splittedDataLine[1].replaceAll("\\[", "").replaceAll("\\]", "");
                        splittedDataLine[1] = splittedDataLine[1].replaceAll(" ", "");
                        Collections.addAll(departments, splittedDataLine[1].split("\\s*,\\s*"));
                        if (parsedFactoryData.containsKey(splittedDataLine[0])) {
                            Collections.addAll(departments, parsedFactoryData.get(splittedDataLine[0]).split("\\s*,\\s*"));
                            parsedFactoryData.put(splittedDataLine[0], departments.toString().replaceAll("\\[", "").replaceAll("\\]", ""));
                            continue;
                        }
                        parsedFactoryData.put(splittedDataLine[0], departments.toString());
                        continue;
                    }
                    if (requiredParsingData.contains(splittedDataLine[0])) {
                        parsedFactoryData.put(splittedDataLine[0], splittedDataLine[1]);
                    }
                }
            }
            ArrayList<FactoryParsingException.FactoryValidationError> factoryValidationErrors = new ArrayList<>();
            Constructor<?> declaredConstructor = factoryClass.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            Factory factory = (Factory) declaredConstructor.newInstance();
            for (Field f : factoryClass.getDeclaredFields()) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(NotBlank.class)) {
                    if (parsedFactoryData.get(f.getName()) == null) {
                        factoryValidationErrors.add(new FactoryParsingException.FactoryValidationError(f.getName(), "Поле не может быть пустым!"));
                        continue;
                    }
                    f.set(factory, parsedFactoryData.get(f.getName()));
                }
                if (f.isAnnotationPresent(Concatenate.class)) {
                    f.set(factory, concatenateString(f.getAnnotation(Concatenate.class).delimiter(),
                            f.getAnnotation(Concatenate.class).fieldNames(), parsedFactoryData));
                    continue;
                }
                if (f.getName().equals("departments")) {
                    if(parsedFactoryData.get(f.getName()) != null){
                        String[] formattedString = parsedFactoryData.get(f.getName()).split("\\s*,\\s*");
                        f.set(factory, Arrays.asList(formattedString));
                        continue;
                    }
                }
                f.set(factory, parsedFactoryData.get(f.getName()));

            }
            checkExceptions(factoryValidationErrors);
            return factory;


        } catch (IOException | ClassNotFoundException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    boolean checkExceptions(ArrayList<FactoryParsingException.FactoryValidationError> exceptions) {
        if (exceptions.isEmpty()) {
            return true;
        }
        throw new FactoryParsingException("При парсинге возникла ошибка", exceptions);
    }

//    HashMap<String,String>
    String concatenateString(String delimiter, String[] requiredConcatenateStrings, HashMap<String,String> parsedData){
        String resultString = "";
        for(String s : requiredConcatenateStrings){
            resultString += parsedData.get(s);
            resultString += delimiter;
        }
        return resultString;
    }
}
